# iOS 12 and Swift. The Complete iOS App Development Bootcamp Challenges
Quest or Q - its app what i maked with partly repeating the actions of the teacher. The source code is partially different from what was in the lectures so as to fit my writing style. It may contain discrepancies with the classical practices of writing code, but it is completely working.
Challenges - Programs that according to the course I had to write myself, using or not using the interface and icon sprites provided to me

Differences btw basic quest:

С - I am Poor:
    Custom sprite
    Costom write
    
Q - Dicee:
    Added shake sound

C - Magic 8 Ball:
    Only quest

C - BMICalculator:
    2 ways to get result

Q - Xylophone:
    Created file Constants.swift what contains list of names of sounds
    Created file SoundController.swift what control sound player
    
Q - Quizzler:
    Custom Launch Screen
    The boundary conditions of the cycle are associated to the size of the list of questions vs just 12 on the lessons

C - Destini:
    Used Object for hold the text.
    Used Switch statement for control text on label and buttons.
    Used Int value for control part of stories
