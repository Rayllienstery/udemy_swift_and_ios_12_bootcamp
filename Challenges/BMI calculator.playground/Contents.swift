import UIKit

func BMICalc (weight: Int, height: Double){ //

    let BMI = (Double(weight)) / (pow(height,Double(2)))

    if BMI > 25 {
        print("you are overweight")
    }else if BMI > 18.5 && BMI < 25 {
        print("you have normal weight")
    }else{
        print("you are underweight")
    }
}

func BMICalc2 (weight: Int, height: Double) -> Double{ //
    return (Double(weight)) / (pow(height,Double(2)))
}

//TESTS WITHOUT RETURN VALUE
BMICalc(weight: 76, height: 1.86)
BMICalc(weight: 102, height: 1.78)
BMICalc(weight: 82, height: 1.90)
BMICalc(weight: 60, height: 1.58)
BMICalc(weight: 47, height: 1.70)

//TESTS WITH RETURN VALUE
var result = BMICalc2(weight: 42, height: 1.38)

if result > 25 {
    print("you are overweight")
}else if result > 18.5 && result < 25 {
    print("you have normal weight")
}else{
    print("you are underweight")
}
