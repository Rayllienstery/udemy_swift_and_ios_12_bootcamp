import Foundation

var width: Float = 1.5
var height: Float = 2.3

var bucketsOfPaint: Int {
    get {
        return Int(ceilf(width * height / 1.5))
    }
    set{
        Int(ceilf(Float(newValue) * 1.5))
    }
}

bucketsOfPaint = 20

print(bucketsOfPaint)
