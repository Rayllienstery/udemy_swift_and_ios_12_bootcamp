//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by Raylee on 1/15/19.
//  Copyright © 2019 Konstantyn Kolosov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var ballSprite: UIImageView!
    
    var ballSpriteContainer = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func askButton(_ sender: Any) {
        randomAnswer()
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        randomAnswer()
    }
    
    func randomAnswer() -> Void {
        
        let randomSpriteNumber = Int.random(in: 0...4)
        ballSprite.image = UIImage(named: ballSpriteContainer[randomSpriteNumber])
        
    }


}

