//
//  ViewController.swift
//  Destini
//
//  Created by Philipp Muellauer on 01/09/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let story = Story()
    
    var storyPart : Int = 0
    
    // UI Elements linked to the storyboard
    @IBOutlet weak var topButton: UIButton!         // Has TAG = 1
    @IBOutlet weak var bottomButton: UIButton!      // Has TAG = 2
    @IBOutlet weak var storyTextView: UILabel!
    
    // TODO Step 5: Initialise instance variables here
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        // TODO Step 3: Set the text for the storyTextView, topButton, bottomButton, and to T1_Story, T1_Ans1, and T1_Ans2
        
    }

    
    // User presses one of the buttons
    @IBAction func buttonPressed(_ sender: UIButton) {
    
        switch storyPart {
        case 0:
            if sender.tag == 1{
                storyPart = 2
            }else if sender.tag == 2{
                storyPart = 1
            }
        case 1:
            if sender.tag == 1{
                storyPart = 2
            }else if sender.tag == 2{
                storyPart = 3
            }
        case 2:
            if sender.tag == 1{
                storyPart = 5
            }else if sender.tag == 2{
                storyPart = 4
            }
        default:
            print("End of story")
        }
        
        updateUI()
    
    }
    
    func updateUI() -> Void {
        storyTextView.text = story.storyParts[storyPart].storyText
        topButton.setTitle(story.storyParts[storyPart].answerAText, for: UIControl.State.normal)
        bottomButton.setTitle(story.storyParts[storyPart].answerBText, for: UIControl.State.normal)
        if(storyPart > 2){
            topButton.isHidden = true
            bottomButton.isHidden = true
        }
    }


}

