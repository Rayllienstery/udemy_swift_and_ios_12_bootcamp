//
//  StoryPart.swift
//  Destini
//
//  Created by Raylee on 1/21/19.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import Foundation

class StoryPart {
    
    let storyText : String
    let answerAText : String
    let answerBText : String
    
    init(_ story: String, _ answerA: String, _ answerB: String) {
        
        storyText = story
        answerAText = answerA
        answerBText = answerB
        
    }
}
