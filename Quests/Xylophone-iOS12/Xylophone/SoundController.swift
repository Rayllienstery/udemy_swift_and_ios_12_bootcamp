//
//  SoundController.swift
//  Xylophone
//
//  Created by Raylee on 1/16/19.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import Foundation

import AVFoundation

public class SoundController{
    
    static var player: AVAudioPlayer?
    
    static func playSound(_ number : Int){
        let url = Bundle.main.url(forResource: soundContainer.noteNumber[number], withExtension: "wav")
        
        do{
            player = try AVAudioPlayer(contentsOf: url!)
            guard let player = player else {return}
            
            player.prepareToPlay()
            player.play()
        }catch let error as Error{
            print(error)
        }
    }
    
}
