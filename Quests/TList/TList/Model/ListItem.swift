//
//  ListItem.swift
//  TList
//
//  Created by Raylee on 7/29/19.
//  Copyright © 2019 TechnomagicFox. All rights reserved.
//

import Foundation
import RealmSwift

class ListItem: Object {
    @objc dynamic var title: String = .init()
    @objc dynamic var status: Bool = false
    @objc dynamic var dateCreated: Date?
    var parentCategory = LinkingObjects(fromType: Category.self, property: "listItems")
}
