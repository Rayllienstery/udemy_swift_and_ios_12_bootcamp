//
//  Category.swift
//  TList
//
//  Created by Raylee on 7/29/19.
//  Copyright © 2019 TechnomagicFox. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var title: String = .init()
    @objc dynamic var color: String = "#ffffff"
   var listItems = List<ListItem>()
}
