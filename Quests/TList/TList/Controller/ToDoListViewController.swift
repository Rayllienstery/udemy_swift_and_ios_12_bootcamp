//
//  ViewController.swift
//  TList
//
//  Created by Raylee on 7/19/19.
//  Copyright © 2019 TechnomagicFox. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework

class ToDoListViewController: SwipeTableViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    let realm = try! Realm()
    
    var listStorage: Results<ListItem>?
    
    var selectedCategory: Category? {
        didSet {
            loadItems()
        }
    }

    @IBOutlet var listTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        loadItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barTintColor =  UIColor.init(hexString: selectedCategory?.color ?? "#ffffff")?.darken(byPercentage: 0.4)
        self.title = selectedCategory?.title ?? "TList"
        self.searchBar.barTintColor =  UIColor.init(hexString: selectedCategory?.color ?? "#ffffff")
        
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: ContrastColorOf((self.navigationController?.navigationBar.tintColor)!, returnFlat: true)]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listStorage?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.backgroundColor = UIColor(hexString: selectedCategory?.color ?? "#ffffff")?.darken(byPercentage: CGFloat(Float(indexPath.row) / Float((listStorage?.count ?? 0))) * 0.3)
        cell.textLabel?.textColor = ContrastColorOf(cell.backgroundColor!, returnFlat: true)
        cell.textLabel?.text = listStorage?[indexPath.row].title ?? "Tasks is empty"
        cell.accessoryType = listStorage?[indexPath.row].status ?? false ? .checkmark : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let item = listStorage?[indexPath.row] {
            do {
                try realm.write {
                    item.status = !item.status
                }
            } catch {
                print(error)
            }
        }
        let cell = tableView.cellForRow(at: indexPath)
        cell!.accessoryType = listStorage?[indexPath.row].status ?? false ? .checkmark : .none
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func updateModel(at indexPath: IndexPath) {
        if let target = self.listStorage?[indexPath.row]{
            self.realm.tryWrite {
                self.realm.delete(target)
                
            }
            print("\(NSStringFromClass(type(of:self))): \(#function) \(indexPath.row) removed")
        }
    }

    @IBAction func addTaskButton(_ sender: Any) {
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add new item", message: nil, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add item", style: .default) { (action) in
            
            if let currentCategory = self.selectedCategory {
                do {
                    try self.realm.write {
                        let listItem = ListItem()
                        listItem.title = textField.text ?? "Empty task"
                        listItem.dateCreated = Date()
                        currentCategory.listItems.append(listItem)
                    }
                }catch {
                    print(error)
                }
                
            }

            self.listTable.reloadData()
        }
        
        alert.addTextField { (alertTextField) in
            textField = alertTextField
            alertTextField.placeholder = "Create new item"
            print(alertTextField.text!)
        }
        
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }
    
    func saveItems(){
        
    }
    
    func loadItems(){
        listStorage = selectedCategory?.listItems.sorted(byKeyPath: "title", ascending: true)
        tableView.reloadData()
    }
    
}

//MARK: - Search bar methods
/***************************************************************/

extension ToDoListViewController: UISearchBarDelegate, UIPickerViewDelegate, UIImagePickerControllerDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        listStorage = listStorage?
            .filter("title CONTAINS[cd] %@", searchBar.text!)
            .sorted(byKeyPath: "dateCreated", ascending: true)
        
        tableView.reloadData()

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.isEmpty ?? true {
            loadItems()

            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }

}
