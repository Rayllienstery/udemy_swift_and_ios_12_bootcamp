//
//  CategoryTableViewController.swift
//  TList
//
//  Created by Raylee on 7/22/19.
//  Copyright © 2019 TechnomagicFox. All rights reserved.
//

import UIKit
import RealmSwift
import SwipeCellKit
import ChameleonFramework

protocol CategoryTableProtocol {
    func saveCategories(category: Category)
    func loadCategories()
}

class CategoryTableViewController: SwipeTableViewController, CategoryTableProtocol {
    
    let realm = try! Realm()
    
    var categories: Results<Category>?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.barTintColor =  UIColor.init(hexString: "#000000")
    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        var textField = UITextField()
        let alert = UIAlertController(title: "Add new category",
                                      message: "",
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "Add", style: .default) { (action) in
            
            let newCategory = Category()
            newCategory.title = textField.text ?? "Empty Category"
            newCategory.color = UIColor.randomFlat.hexValue()
            
            self.saveCategories(category: newCategory)
        }
        
        alert.addAction(action)
        alert.addTextField { (field) in
            textField = field
            textField.placeholder = "Add a new category"
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - TableView Datasource Methods
    /***************************************************************/
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count ?? 1
    }
    
    //MARK: - TableViewDelegate Methods
    /***************************************************************/
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.backgroundColor = UIColor(hexString: categories?[indexPath.row].color ?? "#ffffff")?.darken(byPercentage: 0.3)
        cell.textLabel?.text = categories?[indexPath.row].title ?? "Empty"
        cell.textLabel?.textColor = ContrastColorOf(cell.backgroundColor!, returnFlat: true)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToItems", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! ToDoListViewController
        
        if let indexPath = tableView.indexPathForSelectedRow {
                destinationVC.selectedCategory = categories?[indexPath.row]
        }
    }
    
    
    
    //MARK: - Data Manipulation Methods
    /***************************************************************/
    
    func saveCategories(category: Category) {
        do{
            try realm.write {
                realm.add(category)
                }
        }catch {
            print(error)
        }
        
        tableView.reloadData()
    }
    
    func loadCategories() {
        categories = realm.objects(Category.self)
        tableView.reloadData()
    }
    
    override func updateModel(at indexPath: IndexPath) {
        if
            let target = self.categories?[indexPath.row]{
                            self.realm.tryWrite {
                                self.realm.delete(target.listItems.sorted(byKeyPath: "title", ascending: true))
                                self.realm.delete(target)
                            }
                            print("\(NSStringFromClass(type(of:self))): \(#function) \(indexPath.row) removed")
                        }
    }
    
}

//MARK: - Swipe Cell delegate methods
/***************************************************************/

//extension CategoryTableViewController: SwipeTableViewCellDelegate {
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
//        guard orientation  == .right else { return nil }
//
//        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { (action, indexPath) in
//            if let target = self.categories?[indexPath.row]{
//                self.realm.tryWrite {
//                    self.realm.delete(target)
//                    //tableView.reloadData()
//                }
//                print("\(NSStringFromClass(type(of:self))): \(#function) \(indexPath.row) removed")
//            }
//        }
//
//        deleteAction.image = UIImage(named: "garbage")
//
//        return [deleteAction]
//    }
//
//
//
//
//}

extension Realm {
    func tryWrite(target: () -> Void) {
        do {
            try Realm().write {
                target()
            }
        } catch {
            print(error)
        }
    }
}
