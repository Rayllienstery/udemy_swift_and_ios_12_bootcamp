//
//  ViewController.swift
//  TList
//
//  Created by Raylee on 7/19/19.
//  Copyright © 2019 TechnomagicFox. All rights reserved.
//

import UIKit
import CoreData

class ToDoListViewController: UITableViewController {
    
    var listStorage = [ListItem]()
    var selectedCategory: Category? {
        didSet {
            loadItems()
        }
    }
    let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("Items.plist")
    
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let defaults = UserDefaults.standard

    @IBOutlet var listTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadItems()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listStorage.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "ToDoItemCell")
        
        cell.textLabel?.text = listStorage[indexPath.row].title
        cell.accessoryType = listStorage[indexPath.row].status ? .checkmark : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listStorage[indexPath.row].status = !listStorage[indexPath.row].status
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath)
        cell!.accessoryType = listStorage[indexPath.row].status ? .checkmark : .none
        
        saveItems()
    }

    @IBAction func addTaskButton(_ sender: Any) {
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add new item", message: nil, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add item", style: .default) { (action) in
            let listItem = ListItem(context: self.context)
            listItem.title = textField.text ?? "Empty task"
            listItem.status = false
            listItem.parentCategory = self.selectedCategory
            
            self.listStorage.append(listItem)
            self.saveItems()
            
            self.listTable.reloadData()
        }
        
        alert.addTextField { (alertTextField) in
            textField = alertTextField
            alertTextField.placeholder = "Create new item"
            print(alertTextField.text!)
        }
        
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }
    
    func saveItems(){
        do {
            try context.save()
        }catch{
            print("\(NSStringFromClass(type(of:self))): \(#function) \(error)")
        }
    }
    
    func loadItems(with request: NSFetchRequest<ListItem> = ListItem.fetchRequest(), predicate: NSPredicate? = nil){
        
        var predicateList = NSCompoundPredicate(andPredicateWithSubpredicates: [NSPredicate(format: "parentCategory.name MATCHES %@", selectedCategory!.name!)])
        if let additionalPredicate = predicate {
            predicateList = NSCompoundPredicate(andPredicateWithSubpredicates: [NSPredicate(format: "parentCategory.name MATCHES %@", selectedCategory!.name!), additionalPredicate])
        }
        
        request.predicate = predicateList
        
        do {
            listStorage = try context.fetch(request)
        } catch {
            print("\(NSStringFromClass(type(of:self))): \(#function) \(error)")
        }
        tableView.reloadData()
    }
    
}

//MARK: - Search bar methods
/***************************************************************/

extension ToDoListViewController: UISearchBarDelegate, UIPickerViewDelegate, UIImagePickerControllerDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        let request: NSFetchRequest<ListItem> = ListItem.fetchRequest()
        
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        loadItems(with: request, predicate: NSPredicate(format: "title CONTAINS[cd] %@", searchBar.text!))
        
       
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.isEmpty ?? true {
            loadItems()
            
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
    
}
