//
//  ViewController.swift
//  WeatherApp
//
//  Created by Angela Yu on 23/08/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class WeatherViewController: UIViewController, CLLocationManagerDelegate, ChangeCityDelegate {
    
    //Constants
    let WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather"
    let APP_ID = "5d069217a827e7b4349ca79eed01c54a"
    /***Get your own App ID at https://openweathermap.org/appid ****/
    

    //TODO: Declare instance variables here
    let coreLocation = CLLocationManager()
    let weather = WeatherDataModel()

    
    //Pre-linked IBOutlets
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coreLocation.delegate = self
        coreLocation.desiredAccuracy = kCLLocationAccuracyHundredMeters
        coreLocation.requestWhenInUseAuthorization()
        coreLocation.startUpdatingLocation()
        
    }
    
    
    
    //MARK: - Networking
    /***************************************************************/
    
    //Write the getWeatherData method here:
    
    func getWeatherData(url: String, parameters: [String : String]){
        
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON { (response) in
            if response.result.isSuccess, response.response?.statusCode == 200 {
                self.updateWeatherData(json: JSON(response.result.value!))
                //print("\(NSStringFromClass(type(of:self))): \(#function) \(String(describing: JSON(response.result.value)))")
            } else {
                print("\(NSStringFromClass(type(of:self))): \(#function) \(String(describing:response.result.error))")
            }
        }
        
    }
    
    
    //MARK: - JSON Parsing
    /***************************************************************/
   
    
    //Write the updateWeatherData method here:
    func updateWeatherData(json: JSON){
        weather.temperature = Int((json["main"]["temp"].double ?? 273.15) - 273.15)
        weather.city = json["name"].stringValue
        weather.condition = json["weather"][0]["id"].intValue
        weather.weatherIconName = weather.updateWeatherIcon()
        
        updateUIWithWeatherData()
    }

    
    
    
    //MARK: - UI Updates
    /***************************************************************/
    
    
    //Write the updateUIWithWeatherData method here:
    func updateUIWithWeatherData(){
        cityLabel.text = weather.city
        temperatureLabel.text = String(weather.temperature) + "º"
        weatherIcon.image = UIImage.init(named: weather.weatherIconName)
    }
    
    //MARK: - Location Manager Delegate Methods
    /***************************************************************/
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            coreLocation.stopUpdatingLocation()
            coreLocation.delegate = nil
            
            print("\(NSStringFromClass(type(of:self))): \(#function) lo:\(location.coordinate.longitude) la:\(location.coordinate.latitude)")
            
            let params : [String : String] = ["lat" : String(location.coordinate.latitude), "lon" : String(location.coordinate.longitude), "appid" : APP_ID]
            
            getWeatherData(url: WEATHER_URL, parameters: params)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\(NSStringFromClass(type(of:self))): \(#function) \(error)")
    }
    
    
    //MARK: - Change City Delegate methods
    /***************************************************************/
    
    
    //Write the userEnteredANewCityName Delegate method here:
    func userEnteredANewCityName(city: String) {
        let params: [String: String] = ["q": city, "appid": APP_ID]
        
        getWeatherData(url: WEATHER_URL, parameters: params)
    }

    
    //Write the PrepareForSegue Method here
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeCityName" {
            let destination = segue.destination as! ChangeCityViewController
            destination.delegate = self 
        }
    }
    
    
    
    
}


